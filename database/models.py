from datetime import datetime

import sqlalchemy as sq
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class Type_of_transaction(Base):
    __tablename__ = "Type_of_transaction"

    id = sq.Column(sq.Integer, primary_key=True)
    name = sq.Column(sq.Text)


class User(Base):
    __tablename__ = "User"

    id = sq.Column(sq.Integer, primary_key=True)
    tg_id = sq.Column(sq.BIGINT, nullable=False)
    tg_name = sq.Column(sq.Text)
    name = sq.Column(sq.Text)
    surname = sq.Column(sq.Text)
    patronimic = sq.Column(sq.Text)


class Group(Base):
    __tablename__ = "Group"

    id = sq.Column(sq.Integer, primary_key=True)
    group_tg_id = sq.Column(sq.BIGINT, nullable=False)
    group_name = sq.Column(sq.Text)
    week_limit = sq.Column(sq.BIGINT, nullable=False)
    month_limit = sq.Column(sq.BIGINT, nullable=False)
    week = sq.Column(sq.Integer, nullable=False)


class Transaction(Base):
    __tablename__ = "Transaction"

    id = sq.Column(sq.Integer, primary_key=True)
    week = sq.Column(sq.Integer, nullable=False)
    date = sq.Column(sq.Date, nullable=False)
    amount = sq.Column(sq.Integer, nullable=False)
    type_id = sq.Column(sq.Integer, sq.ForeignKey("Type_of_transaction.id"))
    transaction_by = sq.Column(sq.Integer, sq.ForeignKey("User.id"), nullable=False)
    group_id = sq.Column(sq.Integer, sq.ForeignKey("Group.id"), nullable=False)

    type_of_transaction = relationship(Type_of_transaction, backref="Transaction")
    user = relationship(User, backref="Transaction")
    group = relationship(Group, backref="Transaction")
