import os

import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

from database.models import Base, Type_of_transaction, User, Group, Transaction


class Database:
    def __init__(self):

        # DSN = os.environ.get("FUND_BOT_DB_DSN")

        # DSN = 'postgresql://postgres:postgres@localhost:5432/onboarding_bot_db'

        DSN = "sqlite:///fundbot.db"

        self.engine = sqlalchemy.create_engine(DSN)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def create_tables(self):
        Base.metadata.create_all(self.engine)
        print('Таблицы созданы согласно models.py')

    def is_user(self, tg_id: int) -> bool:
        result = self.session.query(User).filter(User.tg_id == tg_id).first()
        return True if result else False

    def new_user(self, **kwargs):
        try:
            id = max([i.id for i in self.session.query(User).all()]) + 1
        except ValueError:
            id = 1
        user = User(
            id=id,
            tg_id=kwargs.get("tg_id"),
            tg_name=kwargs.get("tg_name"),
            name=kwargs.get("name"),
            surname=kwargs.get("surname"),
            patronimic=kwargs.get("patronimic"),
        )
        try:
            self.session.add(user)
            self.session.commit()
            self.session.close()
            return True
        except IntegrityError:
            self.session.rollback()
            self.session.close()
            return False

    def get_user_by_tg_id(self, tg_id: int) -> object or False:
        result = self.session.query(User).filter(User.tg_id == tg_id).first()
        return result if result else False

    def get_balance_by_id(self, id):
        return self.session.query(Group).filter(Group.id == id).first()

    def get_latest_group(self, group_tg_id: int) -> object or False:
        """
        Return latest week balance.
        """
        result = self.session.query(Group).filter(Group.group_tg_id == group_tg_id).order_by(Group.week.desc()).first()
        return result if result else False

    def lower_balance(self, group_id: int, amount: int) -> bool:
        try:
            old_balance = self.get_latest_group(group_id)
            old_week_balance = old_balance.week_limit
            old_month_balance = old_balance.month_limit
            new_week_balance = int(old_week_balance) - amount
            new_month_balance = int(old_month_balance) - amount
            old_balance.week_limit = new_week_balance
            old_balance.month_limit = new_month_balance
            self.session.commit()
            self.session.close()
            return True
        except:
            self.session.rollback()
            self.session.close()
            return False

    def group_add_new_week(self, **kwargs) -> bool:
        try:
            id = max([i.id for i in self.session.query(Group).all()]) + 1
        except ValueError:
            id = 1
        try:
            new_week = Group(
                id=id,
                group_tg_id=kwargs.get("group_tg_id"),
                group_name=kwargs.get("group_name"),
                week_limit=kwargs.get("week_limit"),
                month_limit=kwargs.get("month_limit"),
                week=kwargs.get("week")
            )
            self.session.add(new_week)
            self.session.commit()
            return True
        except:
            self.session.rollback()
            self.session.close()
            return False

    def new_transaction(self, **kwargs) -> bool:
        """
        Create new_transaction in db.
        :param kwargs:
        week: week nuber (int),
        date: current_date (datetime obj),
        amount: amount of money in transaction (int),
        type: FK to Type_of_transaction (int),
        transaction_by: FK to User (int)
        :return: True if created else False
        """
        try:
            id = max([i.id for i in self.session.query(Transaction).all()]) + 1
        except ValueError:
            id = 1
        try:
            transaction = Transaction(
                id=id,
                week=kwargs.get("week"),
                date=kwargs.get("date"),
                amount=kwargs.get("amount"),
                type_id=kwargs.get("type"),
                transaction_by=kwargs.get("transaction_by"),
                group_id=kwargs.get("group_id"),
            )
            self.session.add(transaction)
            self.session.commit()
            self.session.close()
            return True
        except Exception as ex:
            print(ex)
            self.session.close()
            return False
