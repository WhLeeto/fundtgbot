from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import dp, bot, db
import asyncio


async def balance(message: types.Message):
        if message.from_user.id != message.chat.id:
            balance = db.get_latest_group(message.chat.id)
            if balance:
                week_limit = balance.week_limit
                month_limit = balance.month_limit
                await message.answer(f"Осталось потратить:\n\n"
                                     f"За неделю {week_limit} ₸\n"
                                     f"За месяц {month_limit} ₸")
            else:
                await message.answer("У группы не создан баланс, обратитесь к администратору"
                                     f"Id группы <code>{message.chat.id}</code>", parse_mode=types.ParseMode.HTML)
        else:
            await message.answer("Эта команда только для групп")


def register_handlers_client(dp: Dispatcher):
    dp.register_message_handler(balance, commands="balance")
