from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import dp, bot, db

import asyncio

import datetime

from settings.settings import settings_dict


async def outgo(message: types.Message):
    if message.from_user.id != message.chat.id:
        balance = db.get_latest_group(message.chat.id)
        current_week = datetime.datetime.today().isocalendar()[1]
        try:
            balance_week = balance.week
            if current_week != balance_week:
                new_balance = balance.week_limit + settings_dict["week_amount"] * (current_week - balance.week)
                db.group_add_new_week(
                    group_tg_id=message.chat.id,
                    group_name=message.chat.title,
                    week_limit=new_balance,
                    month_limit=200000,  # todo добавить расчет месячного лимита
                    week=current_week
                )
        except AttributeError:
            new_balance = settings_dict["week_amount"]
            db.group_add_new_week(
                    group_tg_id=message.chat.id,
                    group_name=message.chat.title,
                    week_limit=new_balance,
                    month_limit=200000,  # todo добавить расчет месячного лимита
                    week=current_week
                )

        if message.text.isdigit():
            amount = int(message.text)
            result = db.lower_balance(message.chat.id, amount)
            if result:
                transaction = db.new_transaction(week=current_week,
                                                 date=datetime.datetime.now(),
                                                 amount=amount,
                                                 type_id=1,  # todo создать клавиатуру для типа транзакции
                                                 transaction_by=db.get_user_by_tg_id(message.from_user.id).id,
                                                 group_id=db.get_latest_group(message.chat.id).id,
                                                 )
                if transaction:
                    balance = db.get_latest_group(message.chat.id)
                    await message.answer(f"С баланса списано {amount}\n"
                                         f"Текущий недельный баланс: {balance.week_limit}")
                else:
                    await message.answer("transaction = db.new_transaction() failed")
            else:
                await message.answer("result = db.lower_balance() failed")


def register_handlers_other(dp: Dispatcher):
    dp.register_message_handler(outgo, content_types="text")
