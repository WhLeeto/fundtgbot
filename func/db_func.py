from create_bot import db


def create_user_if_not_exists(tg_id: int, tg_name: int = None, name: str = None, surname: str = None,
                              patronimic: str = None):
    if not db.is_user(tg_id):
        new_user = db.new_user(
            tg_id=tg_id,
            tg_name=tg_name,
            name=name,
            surname=surname,
            patronimic=patronimic,
        )
        if new_user:
            return True
        else:
            return False
    else:
        return True