from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from os import environ

from database.requests import Database


storage = MemoryStorage()

token_tg = environ.get("TOKEN_FUND_BOT")

bot = Bot(token=token_tg)
dp = Dispatcher(bot, storage=storage)

db = Database()
db.create_tables()